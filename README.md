# Wardrobify

Team:

* Sean Cunningham - hats microservice
* David Enriquez - shoes microservice

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

The shoes microservice for the wardrobe application was designed by first establishing models for the api data.

Once the models were established, the poller was then built to poll the data from the wardrobe backend api.

Next, the api view functions were created to handle the HTTP requests for this data. With these functions we were able to show the lists for the models that were created. Insomnia was utilized to verify backend functionality.

The function views were then configured in the URLS.

The react components were built next. We were able to fetch the data and show it in the webpage for the frontend users and developer. React router was also utilized to route the paths with the navigation bar.

## Hats microservice

Models
locationVO: value object to bring location into Hats model. HAs associated encoder LocationVOEncoder;
Hats: contains info on hats (fabric, stylename, color, pic url, locationVO); contains get_api_url function to also provide that data for CRUD needs. has associated HatsListEncoder and HatsDetailEncoder

Polling:
in poll/poller.py
get_locations function: requests data from wardrobe api endpoint. json.loads to convert json. loops through the list of locations and creates or updates LocationVOs for each one.
poll function: while true it executes the get_locations function

API_Views:
in hats_rest/api_views.py
api_list_hats function (GET,POST): check if get or post method. if get it gets and returns all hat objects. if post it loads the request body json so that we can work with it. it uses the location href to grab the locationVO object and updates the location property with it. uses that data to create and return a hat object.
api_show_hat function (GET, UPDATE, DELETE): check if get update or delete. if get it uses the id to get and return the hat object. if delete it uses id to filter for the object and then delete it. if update it uses id to grab the object, then updates any given properties. returns the object.

REACT Components
index: grabs hat data and passes it to App which then can pass to other componenets
HatList: grabs hat data, maps through it, and creates a table row for each hat in the hats table. has a delete componenet added to each line
createFormButton: in table header. loads CreateHatForm
DeleteHatButton: grabs hat id and sends DELETE request to api_show_hat view
UpdateHatButton: grabs hat id and sends UPDATE request to api_show_hat view
CreateHatForm: form that sends POST request to api_hats_list view

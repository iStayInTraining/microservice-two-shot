import { BrowserRouter, Routes, Route, Link } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList';
import CreateHatForm from './CreateHatForm';
import ShoesList from './ShoeList';
import ShoesForm from './CreateShoeForm';

function App({ hats, shoes }) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
            <Route path="/" element={<MainPage />} />
            <Route path="api" >
              <Route path="hats" element={<HatsList hats={hats}/>} />
              <Route path="shoes" element={<ShoesList shoes={shoes}/>} />
            </Route>
            <Route path="hats" >
              <Route path="new" element={<CreateHatForm />} />
            </Route>
            <Route path="shoes" >
              <Route path="new" element={<ShoesForm shoes={shoes}/>} />
            </Route>
          </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

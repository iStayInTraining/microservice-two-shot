import { Link } from 'react-router-dom';

function CreateFormButton({ to }) {
  return <Link to={to}><button>Add a hat</button></Link>;
}


export default CreateFormButton

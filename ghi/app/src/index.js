import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));

async function loadData() {
  try {
    const hatsResponse = await fetch("http://localhost:8090/api/hats");
    const shoesResponse = await fetch("http://localhost:8080/api/shoes/");

    if (!hatsResponse.ok) {
      console.error("Error loading hats list");
    } else if (!shoesResponse.ok) {
      console.error("Error loading shoes list");
    } else {
      const hatsData = await hatsResponse.json();
      const shoesData = await shoesResponse.json();
      root.render(
          <App hats={hatsData.hats} shoes={shoesData.shoes} />
      );
    }
  } catch (error) {
    console.error(error);
  }
}

loadData();

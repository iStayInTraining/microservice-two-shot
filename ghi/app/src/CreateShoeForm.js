import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function ShoesForm({shoes}) {
  const navigate = useNavigate();

  const [binLocations, setBinLocations] = useState([]);
  const [manufacturer, setManufacturer] = useState('');
  const [modelName, setModelName] = useState('');
  const [color, setColor] = useState('');
  const [binLocation, setBinLocation] = useState('');
  const [shoeList, setShoeList] = useState([...shoes]);

  const fetchData = async () =>{
    const url = "http://localhost:8100/api/bins"
    const response = await fetch(url);
    if(!response.ok){
        console.error("error getting bins information")
    } else {
        const data = await response.json();
        for (let bin of data.bins){
            setBinLocations((bins)=>{return [...bins, bin]});
        }
    }
  }

  useEffect(()=>{
    fetchData();
  }, []);



  const handleManufacturerChange = (event) => {
    setManufacturer(event.target.value);
  };

  const handleModelNameChange = (event) => {
    setModelName(event.target.value);
  };

  const handleColorChange = (event) => {
    setColor(event.target.value);
  };

  const handleBinLocationChange = (event) => {
    setBinLocation(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
      data.manufacturer = manufacturer;
      data.model_name = modelName;
      data.color = color;
      data.bin_location = binLocation;
      const shoeUrl = "http://localhost:8080/api/shoes/";
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
        },
      };

      try {
        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {

          setManufacturer('');
          setModelName('');
          setColor('');
          setBinLocation('');

          setShoeList([...shoeList, response]);

          navigate("/api/shoes");
        } else {
          console.error(response);
        }

      } catch (error) {
        console.error(error);
      }
    };

  return (
    <form onSubmit={handleSubmit}>
      <h2>Add a New Shoe</h2>
      {/* Form fields */}
      <div className="mb-3">
        <label htmlFor="manufacturer" className="form-label">
          Manufacturer
        </label>
        <input
          type="text"
          className="form-control"
          id="manufacturer"
          value={manufacturer}
          onChange={handleManufacturerChange}
          required
        />
      </div>
      <div className="mb-3">
        <label htmlFor="modelName" className="form-label">
          Model Name
        </label>
        <input
          type="text"
          className="form-control"
          id="modelName"
          value={modelName}
          onChange={handleModelNameChange}
          required
        />
      </div>
      <div className="mb-3">
        <label htmlFor="color" className="form-label">
          Color
        </label>
        <input
          type="text"
          className="form-control"
          id="color"
          value={color}
          onChange={handleColorChange}
          required
        />
      </div>
      <div className="mb-3">
          <select onChange={handleBinLocationChange} value={binLocation} required id="binLocation" className="form-select" name="binLocation" multiple={false}>
            <option value="">Choose a Bin</option>
            {binLocations.map(bin=>{
              return (
              <option value={bin.href} key={bin.href}>{`${bin.closet_name} / ${bin.bin_number} / ${bin.bin_size}`}</option>
              );
            })}
          </select>
        </div>
      {/* Add more fields if needed */}
      <button type="submit" className="btn btn-primary">
        Add Shoe
      </button>
    </form>
  );
}

export default ShoesForm;

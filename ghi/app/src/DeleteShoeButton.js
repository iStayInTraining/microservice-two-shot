
function DeleteShoeButton({href, handleDelete}) {
  return <button className="btn btn-warning" onClick={()=>handleDelete(href)}>Remove shoe</button>
}
export default DeleteShoeButton

import DeleteHatButton from "./DeleteHatButton"
import { Link } from 'react-router-dom';

import { useState } from "react"

function HatsList({hats}){

    const [list, setList] = useState([...hats])

    async function handleDelete(href){
        const response = await fetch(`http://localhost:8090${href}`, {method: "delete"})
        if(response.ok){
            setList(prevHats=>{
                return prevHats.filter(hat=>hat.href!==href);
            })



        }
      }


    return (
        <div>
        <h2>Hats List</h2>
        <Link to="/hats/new" className="btn btn-primary">Create New Hat</Link>
        <table className="table table-striped">
        <thead>
            <tr>
                <th>Fabric</th>
                <th>Style</th>
                <th>Color</th>
                <th>Location (Closet/Section/Shelf)</th>
                <th></th>
            </tr>
        </thead>
        <tbody>

            {list.map(hat=>{
                return (
                    <tr key={hat.href}>
                    <td>{hat.fabric}</td>
                    <td>{hat.style_name}</td>
                    <td>{hat.color}</td>
                    <td>{`${hat.hat_location.closet_name} / ${hat.hat_location.section_number} / ${hat.hat_location.shelf_number}`}</td>
                    <td>
                        <DeleteHatButton href={hat.href} handleDelete={handleDelete} />
                    </td>
                    </tr>
                )
            })}
          </tbody>
        </table>
        </div>
    )
}

export default HatsList


function DeleteHatButton({href, handleDelete}) {


  return <button className="btn btn-warning" onClick={()=>handleDelete(href)}>Remove Hat</button>
}


export default DeleteHatButton

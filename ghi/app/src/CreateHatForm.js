import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

function CreateHatForm(){
    const navigate = useNavigate();

    const [locations, setLocations] = useState([])
    const [fabric, setFabric] = useState([])
    const [style, setStyle] = useState([])
    const [color, setColor] = useState([])
    const [location, setLocation] = useState([])

    const fetchData = async () =>{
        const url = "http://localhost:8100/api/locations"
        const response = await fetch(url);
        if(!response.ok){
            console.error("error getting location information")
        } else {
            const data = await response.json();
            for (let item of data.locations){
                setLocations((locations)=>{return [...locations, item]});
            }
        }
    }

    useEffect(()=>{
        fetchData();
    }, []);

    const handleFabricChange = (event)=>{
        setFabric(event.target.value);
    }
    const handleStyleChange = (event)=>{
        setStyle(event.target.value);
    }
    const handleColorChange = (event)=>{
        setColor(event.target.value);
    }
    const handleLocationChange = (event)=>{
        setLocation(event.target.value);
    }


    const handleSubmit = async (event)=>{
        event.preventDefault();
        const data = {};
        data.fabric = fabric;
        data.style_name = style;
        data.color = color;
        data.hat_location = location;


        const conferenceURL = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(conferenceURL, fetchConfig);
        if(!response.ok){
            console.error('error loading conference data')
        } else {

            setFabric('');
            setStyle('');
            setColor('');
            setLocation('');

            navigate("/api/hats");

        }
    }



    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new Hat</h1>
            <form onSubmit={handleSubmit} id="create-hat-form">
              <div className="form-floating mb-3">
                <input onChange={handleFabricChange} value={fabric} placeholder="fabric" required type="text" id="fabric" className="form-control" name="fabric" />
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStyleChange} value={style} placeholder="style" required type="text" id="style" className="form-control" name="style" />
                <label htmlFor="style">Style</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColorChange} value={color} placeholder="color" required type="text" id="color" className="form-control" name="color" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} value={location} required id="location" className="form-select" name="location" multiple={false}>
                  <option value="">Choose a Closet (Closet / Section / Shelf)</option>
                  {locations.map(location=>{
                    return (

                    <option value={location.href} key={location.href}>{`${location.closet_name} / ${location.section_number} / ${location.shelf_number}`}</option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default CreateHatForm;

import React from 'react';
import { Link } from 'react-router-dom';
import { useState } from 'react';
import DeleteShoeButton from './DeleteShoeButton';

function ShoeList(props) {
  const [list, setList] = useState([...props.shoes])

    async function handleDelete(href){
        const response = await fetch(`http://localhost:8080${href}`, {method: "delete"})
        if(response.ok){
            setList(prevShoes=>{
                return prevShoes.filter(shoe=>shoe.href!==href);
            });
        }
      }
  return (
    <div>
      <h2>Shoes List</h2>
      <Link to="/shoes/new" className="btn btn-primary">Create New Shoe</Link>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Model Name</th>
            <th>Color</th>
            <th>Bin Location</th>
            {/* Add more columns if needed */}
          </tr>
        </thead>
        <tbody>

          {list.map(shoe => (
            <tr key={shoe.href}>
              <td>{shoe.manufacturer}</td>
              <td>{shoe.model_name}</td>
              <td>{shoe.color}</td>
              <td>{shoe.bin_location.closet_name} - Bin {shoe.bin_location.bin_number}</td>
              <td><DeleteShoeButton href={shoe.href} handleDelete={handleDelete} /></td>
              {/* Adjust the above line based on your BinVO model structure */}
              {/* Add more columns if needed */}
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ShoeList;

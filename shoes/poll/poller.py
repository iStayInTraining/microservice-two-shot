import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()


from shoes_rest.models import BinVO, Shoe


def get_shoes():
    wardrobe_api_url = "http://wardrobe-api:8000/api/bins"

    try:
        response = requests.get(wardrobe_api_url)
        content = response.json()

        if isinstance(content, list):
            bins = content
        elif isinstance(content, dict) and "bins" in content:
            bins = content["bins"]
        else:
            raise ValueError("Invalid response format from the Wardrobe API")

        for bin_data in bins:
            BinVO.objects.get_or_create(
                import_href=bin_data["href"],
                defaults={
                    "closet_name": bin_data["closet_name"],
                    "bin_number": bin_data["bin_number"],
                    "bin_size": bin_data["bin_size"],
                }
            )

    except requests.exceptions.RequestException as e:
        print(f"Error fetching data from the Wardrobe API: {e}")
    except ValueError as e:
        print(e)


def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            get_shoes()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()

from django.db import models
from django.urls import reverse


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, null=True)
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=100)
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=50)
    picture_url = models.URLField()
    bin_location = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
        )

    def get_api_url(self):
        return reverse("api_show_shoe", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.manufacturer} - {self.model_name} - {self.color}"

    class Meta:
        ordering = (
            "manufacturer",
            "model_name",
            "color",
            "picture_url",
            "bin_location",
            )

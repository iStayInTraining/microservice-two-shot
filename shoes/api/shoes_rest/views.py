from json import JSONEncoder
from django.urls import NoReverseMatch
from django.db.models import QuerySet
from datetime import datetime
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import BinVO, Shoe
import json


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        if isinstance(o, self.model):
            d = {}
            if hasattr(o, "get_api_url"):
                try:
                    d["href"] = o.get_api_url()
                except NoReverseMatch:
                    pass
            for property in self.properties:
                value = getattr(o, property)
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                d[property] = value
            d.update(self.get_extra_data(o))
            return d
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}


class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name", "bin_number", "bin_size", "import_href"]

    def get_api_url(self, o):
        return {"api_url": o.get_api_url}


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin_location"
        ]

    def bin_location_to_dict(self, bin_location):
        return {
            "closet_name": bin_location.closet_name,
            "bin_number": bin_location.bin_number,
            "bin_size": bin_location.bin_size,
            "import_href": bin_location.import_href,
        }

    def get_api_url(self, o):
        return {"api_url": o.get_api_url}

    def default(self, o):
        if isinstance(o, BinVO):
            return self.bin_location_to_dict(o)
        else:
            return super().default(o)


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin_location",
    ]
    encoders = {
        "bin_location": BinVODetailEncoder(),
    }

    def get_api_url(self, o):
        return {"api_url": o.get_api_url}


@require_http_methods(["GET", "POST"])
def api_list_shoes(request, id=None):
    if request.method == "GET":
        if id is not None:
            shoes = Shoe.objects.filter(bin=id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
            safe=False,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            bin_location_href = content["bin_location"]
            bin_location = BinVO.objects.get(import_href=bin_location_href)
            content["bin_location"] = bin_location
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin_location"},
                status=400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_shoe(request, pk):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Shoe.objects.filter(id=pk).update(**content)
        hat = Shoe.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

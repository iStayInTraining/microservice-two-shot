# Generated by Django 4.0.3 on 2023-07-19 17:01

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0002_locationvo_alter_hat_hat_location_delete_location'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='locationvo',
            options={},
        ),
    ]

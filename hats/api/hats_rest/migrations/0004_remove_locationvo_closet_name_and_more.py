# Generated by Django 4.0.3 on 2023-07-19 17:15

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0003_alter_locationvo_options'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='locationvo',
            name='closet_name',
        ),
        migrations.RemoveField(
            model_name='locationvo',
            name='section_number',
        ),
        migrations.RemoveField(
            model_name='locationvo',
            name='shelf_number',
        ),
    ]
